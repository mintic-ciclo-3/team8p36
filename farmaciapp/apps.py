from django.apps import AppConfig


class FarmaciappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'farmaciapp'
