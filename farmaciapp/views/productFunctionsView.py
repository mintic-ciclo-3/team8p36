from os import name, putenv
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from farmaciapp.serializers.productSerializer import ProductSerializer
from rest_framework.views import APIView
from farmaciapp.models.product import Product


class ProductList(APIView):
    #request en está variable se tiene todo lo que se está enviando del front 

    def get(self, request, format = None): 
        products = Product.objects.all() # dame todos los productos de la base de datos 
        serializer = ProductSerializer(products, many = True ) #pasar los productos al serializer para que los convierta y  si son varios que los convierta 
        return Response(serializer.data)
    
    def post (self, request, format = None):
        serializer = ProductSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.error, status=status.HTTP_400_BAD_REQUEST)
        


    def put(self, request, pk = name):
        return Response ({'method': 'PUT'})
    
    def patch(self,request,pk = name):
        return Response ({'method': 'PATCH'})
    
#    def delete(self,request,pk= id):
#        serializer = ProductSerializer(data = request.data)
#        serializer = serializer["id"]
#        if serializer == id:
#            serializer.delete()
#            return Response('borrados')
#        return Response('no se borró')
    def delete(self,request,id):
        productos = list(Product.objects.filter(id=id).values())
        print("hola hola hola")


        if len(productos) > 0:
            Product.objects.filter(id=id).delete()      
            datos = {'message': "Success"}
        else:
            datos = {'message':"product not found"}
        return Response(datos)
 