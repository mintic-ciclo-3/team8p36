from os import name
from django.urls import path 
from rest_framework.urlpatterns import format_suffix_patterns
from farmaciapp.views import productFunctionsView

urlpatterns = [
    path('list', productFunctionsView.ProductList.as_view())
]
